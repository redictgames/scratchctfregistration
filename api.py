import json
import random
from emailer import *
import sqlite3
import time
import pbkdf2

alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

sessionExistsTime = 600 
membersInTeam = 4

#Database configuration
database = sqlite3.connect("database.db") # Connect to database and initialize cursor
database.row_factory = sqlite3.Row # Row factory initializing


def _execute(command,params=(),commit=True):
    cursor = database.cursor()
    cursor.execute(command, params)
    data = [el for el in cursor]
    if commit:
        database.commit()
    return data

def password_generator(length):
    pw_length = length
    mypw = ""
    for i in range(pw_length):
        next_index = random.randrange(len(alphabet))
        mypw = mypw + alphabet[next_index]
    return mypw

def get_message(filename):
    a = open(filename, "rt")
    data = a.read()
    a.close()
    return data

def crypt(password):
    return pbkdf2.crypt(password,"scratchkids")


def deleteOldSessions():
    _execute('delete from sessions where (?) > removal_time', (time.time(),),True)

def deleteSession(session):
    _execute('delete from sessions where session=(?) ', (session,),True)

def updateSession(session):
    _execute("update sessions set removal_time=(?) where session=(?)",(time.time()+sessionExistsTime, session),True)

def generateSession():
    return str(random.randint(0, 2**256)) + str(random.randint(0, 2**256))

def getIdBySession(session):
    data = _execute("select user from sessions where session=(?)", (session,))

    if len(data) == 0:
        return 'not found'

    return data[0]["user"]

def createSession(team_id):
    session = generateSession()
    createTime = time.time()

    _execute("insert or replace into sessions (session, user, removal_time) values (?, ?, ?)",
            (session,team_id,createTime+sessionExistsTime),True)
    return session

registration_msg = get_message("registration.txt")

# Main API    
def register(data):
    #username, password, email
    if len(data['username'].split()) == 0 or len(data['password'].split()) == 0 or len(data['email'].split()) == 0:
        return json.dumps({"message":"Все поля обязательны к заполнению"})
    users = _execute("SELECT name FROM users WHERE name=(?)",(data['username'],))
    if len(users) != 0: # Проверка на наличие по
        return json.dumps({"message":"Имя уже используется"})
    emails = _execute("SELECT email FROM users WHERE email=(?)",(data['email'],))
    if len(emails) != 0: # Проверка на наличие по
        return json.dumps({"message":"Почтовый адрес уже используется"})
    activate_key = password_generator(128)
    _execute('INSERT INTO users (name, email, passwordHash,activate_key) VALUES (?,?,?,?)',
        (data['username'],data['email'],crypt(data['password']),activate_key))
    send_email(data['email'],"Regestration on Scratch CTF 2016",registration_msg.format(data['username'], activate_key))
    return json.dumps({"message":"На почту " + data['email'] + " было выслано сообщение с подтверждением."})

def login(data):
    session = json.loads(checkSession(data))
    if session['status'] == True:
        return json.dumps({"message":"Вы уже подключены к системе."})
    #if password == "c8006ac111e1f33cc31bd98b992b39a9d5e08566771aac5f806956bd":
    #    return giveAdmin()
    check = _execute('SELECT passwordHash FROM users WHERE name=(?)',(data['username'],))# Back MD5 password
    if len(check) == 0 or check[0]['passwordHash'] != crypt(data['password']):
        print(check,crypt(data['password']))
        return json.dumps({"message":"Неверный логин или пароль."})
    password = crypt(data['password'])
    activated = _execute('SELECT confemail FROM users WHERE passwordHash=(?)',(password,))[0]# Back MD5 password
    if activated['confemail'] == 0:
        return json.dumps({"message":"Ваш Email не подтвержден."})
    uid  = _execute('SELECT id FROM users WHERE name=(?)',(data['username'],))[0]['id'] # Back MD5 password
    session = createSession(uid)
    return json.dumps({"message":"Вы успешно подключены к системе.", "cookies":{"session":session},"login":True})

def checkSession(data):
    session = _execute('SELECT session FROM sessions WHERE session=(?)',(data['session'],))
    if len(session) != 0:
        return json.dumps({"status":True})
    return json.dumps({"deauth":True,"status":False})

def deauth(data):
    session = json.loads(checkSession(data))
    if session['status']:
        deleteSession(data['session'])
        return json.dumps({"message":"Вы успешно вышли из системы"})
    return json.dumps({"message":"Войдите в свой аккаунт."})

def getTasks(data):
    show_tasks = int(_execute("SELECT value FROM config WHERE name='show_tasks'")[0][0])
    if not show_tasks:
        return json.dumps({"tasks":{}})
    taskgroup = _execute("SELECT value FROM config WHERE name='taskgroup'")[0][0]
    task_list = {}
    tasks = _execute('SELECT points,catalog,name,id,description,hint,author,attachment,contacts FROM tasks WHERE taskgroup=(?)',(taskgroup,)) # Contacts - ?
    uid = getIdBySession(data['session'])
    solved_tasks = _execute('SELECT task FROM attempts WHERE user=(?) and correct=(1)',(uid,))
    team = _execute('select team from users where id=(?)', (uid,))
    if len(team) != 0:
        a = [{'task': i['id']} for i in teamSolved(team[0]['team'], scoreboard=True)]
        solved_tasks.extend(a)
    catalogs = {}
    for task in tasks:
        task = dict(task)
        task['solved'] = task['id'] in [i['task'] for i in solved_tasks]
        if task['catalog'] not in catalogs:
            catalogs.update({task['catalog']:[task]})
        else:
            catalogs[task['catalog']].append(task)
    return json.dumps({"tasks":catalogs})

def getTaskById(task_id):
    return _execute('select * from tasks where id=(?)', (task_id, ))[0]

def teamWrong(team_id):
    members = _execute('select * from users where team=(?)', (team_id,))

    solvedtasks = []

    for member in members:
        memberSolved = _execute('select * from attempts where user=(?) and correct=(0)', (member['id'],))
        solvedtasks.extend(memberSolved)

    tasks = []

    for i in solvedtasks:
        tasks.append({'user':i['user'], 'task': i['task'], 'time': i['time']})

    return tasks

def teamSolved(team_id, scoreboard=False):
    members = _execute('select * from users where team=(?)', (team_id,))

    solvedtasks = []

    for member in members:
        memberSolved = _execute('select * from attempts where user=(?) and correct=(1)', (member['id'],))
        solvedtasks.extend(memberSolved)

    a = []

    for i in solvedtasks:
        if not any([i['task'] == j['task'] for j in a]):
            a.append(i)

    solvedtasks = a
    
    if scoreboard:
        task_ids = [i['task'] for i in solvedtasks]

        tasks = [getTaskById(i) for i in task_ids]

        return tasks

    tasks = []

    for i in solvedtasks:
        task = _execute('select name from tasks where id=(?)', (i['task'],))[0]['name']
        user = _execute('select name from users where id=(?)', (i['user'],))[0]['name']
        points = _execute('select points from tasks where id=(?)', (i['task'],))[0]['points']
        tasks.append({'user':user, 'task': task, 'time': i['time'], 'points': points})

    return tasks

def scoreboard(data):
    teams = []

    for team in _execute("select * from teams"):
        team = dict(team)
        team['points'] = 0
        for task in teamSolved(team['id'], True):
            team['points'] += task['points']
        teams.append(team)

    teams = [{"id":i['id'],"name":i['name'], "points": i['points']} for i in teams]

    teams.sort(key = lambda x: x['points'],reverse=True)

    return json.dumps({'scoreboard':teams})

def createTeam(data):
    uid = getIdBySession(data['session'])

    if len(data['name']) == 0:
        return json.dumps({'message': 'Все поля необходимо заполнить'})
    
    teams = _execute('select * from teams where name=(?)', (data['name'],))
    
    if len(teams) != 0:
        return json.dumps({'message': 'Команда с таким именем уже создана'})

    team = _execute("insert or replace into teams (captain, name) values (?, ?)",
            (uid, data['name']))

    teamid = _execute('select * from teams where captain=(?)', (uid,))[0]['id']

    _execute("update users set team=(?) where id=(?)",(teamid, uid))

    return json.dumps({'message': 'Команда '+data['name']+' успешно создана'})

def sendInvite(data):
    uid = getIdBySession(data['session'])

    team = _execute('select * from teams where captain=(?)', (uid,))
    if len(team) == 0:
        return json.dumps({'message': 'Только капитан может добавлять уастников в команду '})
    team = team[0]

    users = _execute('select * from users where team=(?)', (team['id'],))

    if len(users) == membersInTeam:
        return json.dumps({'message': 'В вашей команде не достаточно мест'})

    invitedUser = _execute('select * from users where email=(?)', (data['email'],))

    if len(invitedUser) == 0:
        return json.dumps({'message': 'Пользователя не существует'})

    if invitedUser[0]['team'] != None:
        return json.dumps({'message': 'Пользователь уже состоит в команде'})

    team = _execute("insert or replace into invites (user, team) values (?, ?)",
            (invitedUser[0]['id'], team['id']))

    return json.dumps({'message': 'Приглашение отправлено'})

def getUsers(data):
    users = _execute('select name, id, team, email from users where confemail=(1)')

    return json.dumps({'users': [dict(i) for i in users]})

def joinTeam(data):
    uid = getIdBySession(data['session'])

    if not any([i['team'] == data['teamid'] for i in _execute('select team from invites where user=(?)', (uid))]):
        return json.dumps({'message': 'Вас не приглашали в эту комагду'})

    users = _execute('select * from users where team=(?)', (data['teamid'],))

    if len(users) == membersInTeam:
        return json.dumps({'message': 'В команде нет мест'})

    _execute("update users set team=(?) where id=(?)",(data['teamid'], uid))

    team = _execute('select name from teams where id=(?)',(data['teamid'],))[0]
    cancelInvite(data)

    return json.dumps({'message': 'Отныне вы состоите в '+team['name']})



def leaveTeam(data):
    uid = getIdBySession(data['session'])
    teamid = _execute("select team from users where id=(?)",(uid,))[0]['team']
    teamCaptain = _execute('select captain from teams where id=(?)', (teamid,))[0]['captain']
    _execute("update users set team=(?) where id=(?)",(None, uid))
    uit = _execute("select * from users where team=(?)",(teamid,))
    
    if len(uit) == 0:
        _execute('delete from teams where id=(?)', (teamid,))
    else:
        if int(uid) == int(teamCaptain):
            newCaptain = random.choice(uit)['id']            
            _execute("update teams set captain=(?) where id=(?)",(newCaptain, teamid))

    return json.dumps({'message': 'Вы покинули команду'})


def setTeamCaptain(data):
    uid = getIdBySession(data['session'])

    team = _execute('select * from teams where captain=(?)', (uid,))

    if len(team) == 0:
        return json.dumps({'message': 'Вы не являетесь капитаном этой команды'})

    team = team[0]

    user=_execute('select * from users where id=(?)', (data['uid'],))

    if len(user) == 0:
        return json.dumps({'message': 'Пользователя не существует'})

    user = user[0]

    if user['team'] != team['id']:
        return json.dumps({'message': "Пользователь не состоит в команде"})

    _execute("update teams set captain=(?) where id=(?)",(user['id'], team['id']))

    return json.dumps({'message': 'Теперь '+user['name']+" Ваш капитан"})

def kickFromTeam(data):
    uid = getIdBySession(data['session'])
    team = _execute('select * from teams where captain=(?)', (uid,))

    if len(team) == 0:
        return json.dumps({'message': 'Вы не являетесь капитаном этой команды'})

    team = team[0]
    user=_execute('select * from users where id=(?)', (data['uid'],))

    if len(user) == 0:
        return json.dumps({'message': 'Пользователя не существует'})

    user = user[0]

    if user['team'] != team['id']:
        return json.dumps({'message': "Пользователь не состоит в команде"})

    _execute("update users set team=(?) where id=(?)",(None, user['id']))

    return json.dumps({'message': user['name']+" Больше не состоит в команде"})

def renameTeam(data):
    uid = getIdBySession(data['session'])
    team = _execute('select * from teams where captain=(?)', (uid,))

    if len(team) == 0:
        return json.dumps({'message': 'Вы не являетесь капитаном этой команды'})

    team = team[0]
    _execute("update teams set name=(?) where id=(?)",(data['name'], team['id']))

    return json.dumps({'message': "Название команды изменено на "+data['name']})

def usersInTeam(data):
    team = _execute('select * from teams where id=(?)', (data['teamid'],))
    if len(team) == 0:
        return json.dumps(None)
    users = _execute('select name, id, email, team from users where team=(?)', (data['teamid'],))
    return json.dumps({'users': [dict(i) for i in users]})

def sendFlag(data):
    if type(data['flag']) != type(""):
        return json.dumps({"message":"Флаг должен быть строкой."})

    user = getIdBySession(data['session'])
    solved_tasks = _execute('SELECT * FROM attempts WHERE user=(?)',(user,))

    u = _execute('select * from users where id=(?)', (user,))[0]
    if u['team'] != None:
        uit = _execute('select * from users where team=(?)', (u['team'],))
        for i in uit:
            solved_tasks.extend(_execute('select * from attempts where user=(?)', (i['id'],)))

    for item in solved_tasks:
        if str(data['taskid']) == str(item['task']) and item['correct']:
            return json.dumps({"message":"Флаг уже был сдан","solved":True})

    task = _execute('SELECT flag,taskgroup FROM tasks WHERE id=(?)',(data['taskid'],))[0]
    group = _execute('SELECT value FROM config WHERE name="taskgroup"')[0][0]
    if task['taskgroup'] != group:
        return json.dumps({"message":"Ошибка. Уберите свои руки.", "solved":False})
    if task['flag'] == data['flag']:
        _execute('INSERT INTO attempts (task,user,time,correct,flag) VALUES (?,?,?,?,?)',(data['taskid'],user,time.time(),1,data['flag']))
        return json.dumps({"message":"Флаг был успешно сдан","solved":True})
    _execute('INSERT INTO attempts (task,user,time,correct,flag) VALUES (?,?,?,?,?)',(data['taskid'],user,time.time(),0,data['flag']))
    return json.dumps({"message":"Неверный флаг","solved":False})

def getTeamInfo(data):
    team = _execute('select * from teams where id=(?)', (data['teamid'],))
    if len(team) == 0:
        return json.dumps(None)
    team = team[0]
    users = _execute('select name, id, email, team from users where team=(?)', (data['teamid'],))
    info = {'users': [dict(i) for i in users]}
    info['name'] = team['name']
    info['captain'] = team['captain']
    info['teamSolved'] = teamSolved(team['id'])
    info['wrong'] = teamWrong(team['id'])
    info['id'] = team['id']
    return json.dumps({'team': info})

def getMyTeam(data):
    uid = getIdBySession(data['session'])

    teamid = _execute('select team from users where id=(?)', (uid,))[0]['team']

    if teamid == None:
        return json.dumps({'team': None})

    data['teamid'] = teamid

    return getTeamInfo(data)

def me(data):
    uid = getIdBySession(data['session'])

    user = _execute('select * from users where id=(?)', (uid,))[0]

    return json.dumps({'email': user['email'], 'id': user['id'], 'name': user['name']})

def getInvites(data):
    uid = getIdBySession(data['session'])

    invites = _execute('select * from invites where user=(?)', (uid,))

    a = []

    for i in invites:
        teamname = _execute('select * from teams where id=(?)', (i['team'],))
        if len(teamname) != 0:
            a.append({'team': i['team'], 'teamname': teamname[0]['name']})
        else:
            _execute('delete from invites where team=(?)', (i['team'],))

    return json.dumps({'invites': a})

def cancelInvite(data):
    uid = getIdBySession(data['session']) 

    invites = _execute('select * from invites where user=(?) and team=(?)', (uid, data['teamid']))

    if len(invites) == 0:
        return json.dumps({'message': 'Такого приглашения нет'})

    _execute('delete from invites where user=(?) and team=(?)', (uid, data['teamid']))

    return json.dumps({})

def getNews(data):
    news = []
    adminNews = _execute('select * from news')

    tasks = _execute('select task,user,time from attempts where correct=1')
    tasks.sort(key=lambda x: x['time'])
    filteredTasks = []

    for i in tasks:
        if all([j['task'] != i['task'] for j in filteredTasks]):
            filteredTasks.append(i)

    def taskMap(x):
        task = getTaskById(x['task'])['name']
        teamId = _execute('select team from users where id=(?)', (x['user'],))[0]['team']

        if teamId == None:
            return {
            'time': x['time'],
            'task': task,
            'team': None,
            'id': None
        }

        team = _execute('select name from teams where id=(?)', (teamId,))[0]['name']

        return {
            'time': x['time'],
            'task': task,
            'team': team,
            'id': teamId
        }


    filteredTasks = map(taskMap, filteredTasks) 

    for i in filteredTasks:
        if i['team'] != None:
            news.append(i)

    news.extend([dict(i) for i in adminNews])

    return json.dumps({'news': news})

methods = {
'register': [register,["username","password","email"]],
'login': [login,["username","password"]],
'checkSession': [checkSession,["session"]],
'deauth':[deauth,["session"]],
'getTasks':[getTasks,["session"]],
'scoreboard':[scoreboard,["session"]],
'createTeam':[createTeam,["session", 'name']],
'sendInvite':[sendInvite,["session", 'email']],
'getUsers':[getUsers,["session"]],
'joinTeam':[joinTeam,["session", 'teamid']],
'leaveTeam':[leaveTeam,["session"]],
'setTeamCaptain':[setTeamCaptain,["session", 'uid']],
'kickFromTeam':[kickFromTeam,["session", 'uid']],
'renameTeam':[renameTeam,["session", 'name']],
'usersInTeam':[usersInTeam,["session", 'teamid']],
'teamSolved':[lambda data: json.dumps(teamSolved(data['teamid'])),['session', 'teamid']],
'sendFlag':[sendFlag,["session","taskid","flag"]],
'getTeamInfo':[getTeamInfo,["session","teamid"]],
'getMyTeam':[getMyTeam,['session']],
'me':[me,['session']],
'getInvites':[getInvites,['session']],
'cancelInvite':[cancelInvite,['session', 'teamid']],
'getNews':[getNews, ['session']]
}


admin = {
    
}
