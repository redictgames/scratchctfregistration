import json
import random
import sqlite3
from bottle import redirect,get, post, request,response, run, error, HTTPError, static_file, install,Bottle,hook,template
import time
from gevent import monkey; monkey.patch_all()
from wsgilog import WsgiLog
import api
import datetime

import os

if not os.path.exists('logs'):
	os.mkdir('logs')

app = Bottle()

root = 'src'
# Main GET requests and HOOKs
@app.hook('before_request')
def remove():
	api.deleteOldSessions()

@app.hook('after_request')
def log():
	request.environ['wsgilog.logger'].info(request)

@app.get("/",reset=True)
def get_index():
	session = request.get_cookie('session')
	if session == None or not json.loads(api.checkSession({"session": session}))['status']:
		return redirect('/welcome')

	return static_file('index.html',root="src")

@app.get("/welcome")
def landing():
	return static_file("index.html",root="landing")

@app.get('/welcome/<filename:path>')
def landing_static(filename):
	return static_file(filename,root="landing")

@app.get('/admin/<filename:path>')
def admin_static(filename):
	return static_file(filename,root="adminboard")

@app.get('/attachments/<filename:path>')
def server_attachments(filename):
	return static_file(filename,root="attachments")

@app.get('/<filename:path>')
def server_static(filename):
	return static_file(filename,root="src")

@app.get('/admin',reset=True)
def get_admin():
	return static_file("index.html",root="adminboard")

admins=[]

@app.post("/adminapi")
def adminapi():
	session = request.get_cookie('adminsession')
	requestment = request.body.read().decode('utf-8')
	data = json.loads(requestment)
	if data['method'] == 'login':
		if data['password'] == 'Aclopzslr6CoUNZzhezQDg5dC5nAKcSM':
			session = api.password_generator(32)
			admins.append(session)
			return json.dumps({'session': session})
		else:
			return json.dumps({"message":"GTFO", "deauth":True})
	admin_check = 1 if session in admins else 0
	if admin_check == 0:
		return json.dumps({"message":"GTFO", "deauth":True})
	if data['method'] == 'createNews':
		api._execute("insert or replace into news (text,time) values (?, ?)",(data['news'],data['date']),True)
		return json.dumps({"message":"Новость добавлена!"})
	if data['method'] == 'changeNews':
		if data['action'] == "DELETE":
			api._execute("DELETE FROM news WHERE id=(?)",(data['id'],),True)
			return json.dumps({"message":"Новость была удалена"})
		elif data['action'] == "UPDATE":
			api._execute("UPDATE news SET text = (?) WHERE id=(?)", (data['news'], data['id']),True)
			api._execute("UPDATE news SET time = (?) WHERE id=(?)", (data['date'], data['id']),True)
			return json.dumps({"message":"Новость была успешно обновлена"})
	if data['method'] == "getNews":
		a = api._execute("SELECT * FROM news")
		result = [dict(i) for i in a]
		return json.dumps({"news":result})

@app.error(404)
def error_handle(error):
	return static_file("404.html", root=root)

@app.get("/activate")
def activate():
	activation_key = request.GET['key']
	check = api._execute("SELECT activate_key FROM users WHERE activate_key=(?)",(activation_key,))
	if len(check) == 0:
		return json.dumps({"message":"Ошибка. Неверный активационный ключ."})
	activated = api._execute('SELECT confemail FROM users WHERE activate_key=(?)',(activation_key,))
	if activated[0]['confemail'] == 1:
		return json.dumps({"message":"Ваш Email уже подтвержден."})
	api._execute('UPDATE users SET confemail=(1) WHERE activate_key=(?)',(activation_key,),True)
	response.status = 303
	response.set_header('Location', '/')
	return response

#API
@app.post("/api",reset=True)
def ServerApi():
	session = request.get_cookie('session')

	requestment = request.body.read().decode('utf-8')
	api.updateSession(session)
	data = json.loads(requestment)
	data['session'] = session
	if len(data) == 0:
		return json.dumps({"message":"Отсутствуют параметры для запроса"})
	if 'method' not in data.keys():
		return json.dumps({"message":"Отсутствуют метод"})
	request.environ['wsgilog.logger'].info([data,"UserSession: " + str(session),"IP: " + request['REMOTE_ADDR']])
	params = list(data.keys())
	params.remove("method")
	if data['method'] not in api.methods.keys():
		return json.dumps({"message":"Несуществующий метод"})
	for arg in api.methods[data['method']][1]:
		if arg in params:
			continue
		return json.dumps({"message":"Неверные параметры запроса"})
	if 'session' in api.methods[data['method']][1]:
		session = json.loads(api.checkSession(data))
		if not session['status']:
			return redirect('/welcome')
	return api.methods[data['method']][0](data)
	
app = WsgiLog(app,tofile=True, file="logs/{0}.log".format(datetime.date.today().strftime("%d-%m-%Y")),tostream=True)

if __name__ == "__main__":
	try:
		run(app,host="0.0.0.0",port='80',server="gevent")
	except KeyboardInterrupt:
		raise SystemExit
	finally:
		api.database.commit()
		api.database.close()
		raise SystemExit
		pass
