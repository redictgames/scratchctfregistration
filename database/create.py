import sqlite3

database = sqlite3.connect("database.db")
database.row_factory = sqlite3.Row

for i in open('database.sql', 'r').read().split(';'):
    database.execute(i)

database.commit()

