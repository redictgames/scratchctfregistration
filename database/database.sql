CREATE TABLE "users" (
	`name`	text,
	`id`	integer PRIMARY KEY AUTOINCREMENT,
	`team`	integer,
	`passwordHash`	text,
	`email`	text,
	`activate_key`	TEXT,
	`confemail`	INTEGER DEFAULT 0
);
CREATE TABLE "teams" (
	captain integer,
	id integer PRIMARY KEY AUTOINCREMENT,
	name text
);
CREATE TABLE "tasks" (
	`id`	integer PRIMARY KEY AUTOINCREMENT,
	`name`	text,
	`hint`	text,
	`author`	text,
	`description`	text,
	`attachment`	text,
	`contacts`	text,
	`points`	integer,
	`catalog`	TEXT,
	`flag`	TEXT
);
INSERT INTO `tasks` VALUES (1,'Encrypted Packet',NULL,'nurik040404','Компания шифрует данные с помощью специальной программы(encryptor.py), нам требуется расшифровать пакет данных, который мы перехватили.','["attachments/Encrypted_packet.rar"]','https://vk.com/0x6a6170',50,'ppc','flag{xor_is_cool}');
INSERT INTO `tasks` VALUES (2,'Shufflin'' is good',NULL,'blackmius','Наш сотрудник пропал, вот, что было у него на столе.','["attachments/Shufflin_is_good.png"]','https://telegram.me/Blackmius',100,'crypto','flag{24attemps}');
INSERT INTO `tasks` VALUES (3,'Kumir','["flag{}.lowercase()"]','blackmius','Возможно это координаты их главного сервера, но на карте я их не нашел, что это?','["attachments/Kumir.txt"]','https://telegram.me/Blackmius',150,'ppc','flag{7ur7le}');
INSERT INTO `tasks` VALUES (4,'Pixel is hidden',NULL,'blackmius','Ой, я что-то натворил с этой картинкой!','["attachments/Pixel_is_hidden.rar"]','https://telegram.me/Blackmius',100,'misc','flag{C4PTCH4}');
INSERT INTO `tasks` VALUES (5,'Reverse-Engeneer',NULL,'nurik040404','Мы нашли файл. Как только мы его открыли, он закрылся и больше не открывался, что с ним?','["attachments/Reverse_engeneer.exe"]','https://vk.com/0x6a6170',200,'reverse','flag{reverse_here}');
INSERT INTO `tasks` VALUES (6,'Brightness',NULL,'blackmius','Боже, они придумали новый вид шифрования, что делать?!','["attachments/Brightness.jpg"]','https://telegram.me/Blackmius',100,'stegano','flag{U_C4nt_See_m3}');
INSERT INTO `tasks` VALUES (7,'DJ Encoder','["flag{}.lowercase()"]','nurik040404','Мы нашли странную программу на JavaScript, но тут какие-то кракозябры...','["attachments/encoded.js"]','https://vk.com/0x6a6170',150,'crypto','flag{jjencoder}');
INSERT INTO `tasks` VALUES (8,'Encoding force',NULL,'blackmius','Я нашел у себя на компьютере проект, которым я давно шифровал строки, но забыл как он работает, поможешь расшифровать эту строку - galfced{tpyr_noi}dog ?','["attachments/encoding_force.rar"]','https://telegram.me/Blackmius',50,'crypto','flag{decryption_god}');
INSERT INTO `tasks` VALUES (9,'RSA-kiddies',NULL,'blackmius','Данные, которые отправлялись этой компанией зашифрованы алгоритмом RSA, мы достали несколько приватных ключей, но не знаем, какой из них требуется для дешифровки.','["attachments/RSA-kiddies.rar"]','https://telegram.me/Blackmius',100,'crypto','flag{PrivateKey}');
INSERT INTO `tasks` VALUES (10,'Textitution',NULL,'nurik040404','Мы перехватили странное сообщение, нам нужно его разобрать.','["attachments/textitution.txt"]','https://vk.com/0x6a6170',100,'crypto','flag{substitution_king}');
INSERT INTO `tasks` VALUES (11,'AnonymousCrew',NULL,'blackmius','Эти чуваки из AnonymousCrew, какие-то странные, нужно все разузнать у них.','["http://127.0.0.1:8002"]','https://telegram.me/Blackmius',75,'joy','flag{Ch4tB07}');
INSERT INTO `tasks` VALUES (12,'Scratch out your eyes',NULL,'blackmius','Это делали школьники? Не похоже...','["attachments/scratch.sb2"]','https://telegram.me/Blackmius',150,'joy','flag{5cr47ch_kid5}');
INSERT INTO `tasks` VALUES (13,'Ladder of depth',NULL,'blackmius','Среди нас завелся предатель, чтобы его найти, нужно понять, что он тут наделал...','["attachments/Ladder_of_depth.rar"]','https://telegram.me/Blackmius',100,'misc','flag{scubaDiver}');
INSERT INTO `tasks` VALUES (14,'Code puzzle',NULL,'nurik040404','Их программист смешал код чтобы его никто не разобрал, нужно собрать его обратно!','["attachments/code_puzzle.py"]','https://vk.com/0x6a6170',150,'ppc','flag{pythonprogrammer}');
INSERT INTO `tasks` VALUES (15,'Password-Leak',NULL,'nurik040404','Мы нашли программу, которая требует пароль, но мы не можем подобрать его уже третий день!','["attachments/password_leak.exe"]','https://vk.com/0x6a6170',200,'reverse','flag{p455w0rd_l34k_n0t_h4rd}');
INSERT INTO `tasks` VALUES (16,'Time Stamp',NULL,'blackmius','Что это за кодировка, я впервые ее вижу!','["attachments/Time_Stamp.rar"]','https://telegram.me/Blackmius',100,'reverse','flag{li77leP13ces}');
INSERT INTO `tasks` VALUES (17,'Colorful Message',NULL,'blackmius','Картинка зашифрована странным алгоритмом, мы не знаем, что делать...','["attachments/Colorful_Message.png"]','https://telegram.me/Blackmius',75,'stegano','flag{L1nux}');
INSERT INTO `tasks` VALUES (18,'Strange Image',NULL,'blackmius','Мы обнаружили странную картинку, что с ней не так?','["attachments/Strange_Image.jpg"]','https://telegram.me/Blackmius',100,'stegano','flag{lolrarJpeg}');
INSERT INTO `tasks` VALUES (19,'Stupid admin',NULL,'nurik040404','Их бывший администратор был настолько глуп, что оставил входные данные где-то тут','["http://127.0.0.1:8003"]','https://vk.com/0x6a6170',75,'web','flag{wrong_hole}');
INSERT INTO `tasks` VALUES (20,'Terminal Combination',NULL,'blackmius','Наша команда обнаружила терминал к которому, мы сейчас подбираем пароль.','["attachments/Terminal_Combination.rar"]','https://telegram.me/Blackmius',125,'web','flag{bruteforcemaster}');
CREATE TABLE "sessions" (
	`session`	text,
	`user`	text,
	`removal_time`	integer
);
CREATE TABLE news (
	id integer PRIMARY KEY AUTOINCREMENT,
	time integer,
	text text
);
CREATE TABLE invites (
	user integer,
	team integer
);
CREATE TABLE "attempts" (
	id integer PRIMARY KEY AUTOINCREMENT,
	task integer,
	user integer,
	time integer,
	correct boolean,
	flag text
);
