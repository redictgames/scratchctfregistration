from bottle import get, post, request, run, error, HTTPError, static_file

root = 'site'

@get("/")
def get_index():
	return static_file('index.html',root=root)

def get_error():
	return static_file('error.html',root=root)

@get("/robots.txt")
def robots():
	return static_file('robots.txt',root='')

@get("/AdminPanel.html")
def admin_panel():
	return static_file('AdminPanel.html',root=root)

@get('/<filename>.css')
def server_static(filename):
    return static_file(filename+".css",root=root)

@post("/")
def login():
	return get_error()

@error(404)
@error(500)
def error_handle(error):
	return get_index()

if __name__ == "__main__":
	run(host="0.0.0.0",port='8001', post="8001", debug=True)