with open('words.txt', 'r') as f:
	words = f.read().lower().split('\n')
f.close()

div = lambda arr, n: [arr[i:i+n] for i in range(0, len(arr), n)]

words =  [[i[1].split(','), i[0]] for i in div(words, 2)]

replacements = [
	('a', '4'), 
	('e', '3'), 
	('g', '6'), 
	('i', '!'), 
	('j', '9'), 
	('l', '1'), 
	('o', '0'), 
	('s', '5'), 
	('t', '7'),
	('z', '2')
]

replacements = words + replacements

def translate(string):
	for old, new in replacements:
		for i in old:
			string = string.replace(i, new)

	return string