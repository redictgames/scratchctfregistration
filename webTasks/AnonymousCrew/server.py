from bottle import get, post, request, run, error, HTTPError, static_file
from chatterbot import ChatBot
from leetTranslator import translate
import json

chatbot = ChatBot(
	"Anonymous",
		logic_adapters=[
		"chatterbot.adapters.logic.TimeLogicAdapter",
		'chatterbot.adapters.logic.ClosestMatchAdapter'
		]
	)

root = 'site'

flag = 'flag{Ch4tB07}'

@get("/")
def get_index():
	return static_file('index.html',root=root)

def get_error():
	return static_file('error.html',root=root)

@get('/<filename>.css')
def server_static(filename):
    return static_file(filename+".css",root=root)

@get('/<filename>.js')
def server_static(filename):
    return static_file(filename+".js",root=root)

@post("/")
def answer():
	data = json.loads(request.body.read())

	if data['message'] in ['give me a flag', 'give me flag', 'give flag']:
		return json.dumps({'message': flag})

	bot_answer = chatbot.get_response(data['message'])

	with open('botLog.log', 'a') as f:
		f.write('human: ' + data['message'] + '\n')
		f.write('bot: ' + bot_answer + '\n')
		
	f.close()

	return json.dumps({'message': translate(bot_answer)})

@error(404)
@error(500)
def error_handle(error):
	return get_index()

if __name__ == "__main__":
	run(host="0.0.0.0",port='8002', debug=True)