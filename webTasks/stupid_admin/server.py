from bottle import get, post, request, run, error, HTTPError, static_file

root = 'site'

@get("/")
def get_index():
	return static_file('index.html',root=root)

@get('/<filename>.css')
def server_static(filename):
    return static_file(filename+".css",root=root)

@post("/")
def login():
	login = request.forms.get('login')
	password = request.forms.get('password')
	if login.lower() == "admin":
		if password == "leavemealone":
			return static_file("success.html",root=root)
	return get_index()

@error(404)
@error(500)
def error_handle(error):
	return get_index()

if __name__ == "__main__":
	run(host="0.0.0.0",port='8003', post="8003", debug=True)