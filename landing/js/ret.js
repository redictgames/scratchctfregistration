var ret = function(resp) {
	var resp = JSON.parse(resp) || {'error': {'code': 'null response'}}

    if(resp.message) {
        snackbar.MaterialSnackbar.showSnackbar({message:resp.message, timeout:1500})
    }

	if(resp.error) {
		var error = resp.error
		console.log(error)

		if(error = 'auth_error') {
			auth.$m.show()
		}
	}

	if(resp.cookies) {
		var cookies = resp.cookies

		for(var i in cookies) {
			Cookies.set(i, cookies[i])
		}		
	}

	return resp
}
