Array.prototype.random = function() {return this[Math.floor((Math.random() * this.length))]}

var swapArrayElements = function(arr, indexA, indexB) {
  var temp = arr[indexA];
  arr[indexA] = arr[indexB];
  arr[indexB] = temp;
};

Array.prototype.swap = function(indexA, indexB) {swapArrayElements(this, indexA, indexB);};
   
var crown = ['♛', '♕']//.random()

var isFirefox = typeof InstallTrigger !== 'undefined';

google.charts.load('current', {'packages':['corechart']});

scratchCTF = {
	scoreboard: {},
	tasks: {},
	myteam: {},
	news: []
}


teamTab = $('#teamTab')
scoreboardTab = $('#scoreboardTab')
tasksTab = $('#tasksTab')

var tabs = [
	teamTab,
	scoreboardTab,
	tasksTab
]

var tabActives = function() { 
	for (var i=0; i<tabs.length; i++) {
		if (tabs[i] != this && tabs[i].hasClass('active')) {
			tabs[i].removeClass('active')
		}
	}
}

for (var i in tabs) {
	tabs[i].bind('click', tabActives);
}

function getByValue(arr, key, value) {

  var result  = arr.filter(function(o){return o[key] == value;} );

  return result? result[0] : null; // or undefined

}

function myTeam() {
	if (scratchCTF.me != null) {
		var nick = scratchCTF.me.name
	} else {
		ApiMethods.me()
		var nick = ''
	}
	 
	if (scratchCTF.myteam.team == null) {
		var databadge = 'data-badge="'+((!!scratchCTF.invites&&scratchCTF.invites.length > 0)?String(scratchCTF.invites.length):'')+'"'
		var badge = '<div class="material-icons mdl-badge mdl-badge--overlap" style="position: absolute; right: -1;"'+databadge+'></div>'
		teamTab.html('<a data-toggle="tab" href="#teamPage"  onclick="routie(\'noTeam\')">'+nick+((!!scratchCTF.invites&&scratchCTF.invites.length > 0)?badge:'')+'</a>')
	} else {
		teamTab.html('<a data-toggle="tab" href="#teamPage" onclick="routie(\'team/\'+scratchCTF.myteam.team.id)">'+nick+' Команда:'+scratchCTF.myteam.team.name+' Очки:'+getByValue(scratchCTF.scoreboard, 'id', scratchCTF.myteam.team.id).points+'</a>')
	}
}

var page = gi([]);

var content = function() {
    return gi({is: "div.page"}, page);
};

var update = function() {
    cito.vdom.update(rootEl, content)
};

window.addEventListener("resize", update)

var rootEl = cito.vdom.append(document.body, content);

function updateData() {
	ApiMethods.scoreboard()
	ApiMethods.getTasks()
	ApiMethods.getMyTeam()
	ApiMethods.me()	
	ApiMethods.getInvites()
	ApiMethods.getNews()	
}

updateData()
setInterval(updateData, 10000)

function clearScreen() {
	page = gi([])
	update()
}

function toScoreboard() {
	clearScreen()
	page = $gi.scoreboard
	ApiMethods.scoreboard()
}

function toTasks() {
	clearScreen()
	page = $gi.tasksPage
	ApiMethods.getTasks()
}

function toTeam(id) {
	clearScreen()
	scratchCTF.teamid = id
	page = $gi.teamPage
	ApiMethods.getTeamInfo(id)	
}

function toNoTeam() {
	clearScreen()
	page = $gi.noTeamPage
	ApiMethods.getInvites()
}

function toNews() {
	clearScreen()
	page = $gi.news
	ApiMethods.getNews();
}

routie('', toNews);

routie({
	'scoreboard': toScoreboard,
	'tasks': toTasks,
	'noTeam': toNoTeam,
	'team/:id': (id) => toTeam(Number(id)),
	'news': toNews
});
