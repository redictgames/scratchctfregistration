var DEBUG = false

var ret = function(resp) {
	if (DEBUG) console.log(resp)
	var resp = JSON.parse(resp) || {'error': {'code': 'null response'}}

    if(resp.message) {
        snackbar.show({message:resp.message, timeout:resp.timeout || undefined})
    }

	if (resp.deauth) {
		Cookies.remove('session')
		Cookies.remove('state')
		window.location = '/welcome'
	}

	if(resp.cookies) {
		var cookies = resp.cookies

		for(var i in cookies) {
			Cookies.set(i, cookies[i])
		}		
	}

	return resp
}

var api = (function() {
    var noop = function() {};
    var self = {
        default_onFail: noop,
        default_onSuccess: noop,
        default_onResponse: noop,
        request: function(data) {
            var c = {
                onResponse: function(fn) {c._onResponse = fn; return c},
                onSuccess: function(fn) {c._onSuccess = fn; return c},
                onFail: function(fn) {c._onFail = fn; return c},
                _onResponse: noop,
                _onSuccess: noop,
                _onFail: noop,
                send: function() {
                    $.ajax({
					    url: '/api',
					    type: "POST",
					    data: JSON.stringify(data),
					    contentType: "application/json",
					    crossDomain: true,
					    success: function(rt) {
					    	c.response = ret(rt) 
					    	c._onSuccess(c.response)					    					    	
					    },
					    error: function(rt) {
					    	c.error = rt
					    	c._onFail(c.error)
					    },
					    complete: function(rt) {
					    	c.complete = rt
					    	c._onResponse(c.complete)
					    	if (data.method != 'checkSession') update()						    	
					    }
					});
                }
            };
            return c;
        }
    };
    return self;
}());

ApiMethods = {
	checkSession: function() {
		var a = api.request({method: 'checkSession'}).send()
	},
	scoreboard: function() {
		var a = api.request({method: 'scoreboard'}).onSuccess(function(ret){scratchCTF.scoreboard=ret.scoreboard;myTeam()})
		a.send()
		return a
	},
	createTeam: function(name) {
		var a = api.request({method: 'createTeam', name: name}).send()
		ApiMethods.scoreboard()
		ApiMethods.getMyTeam(true)
	},
	sendInvite: function(email) {
		var a = api.request({method: 'sendInvite', email: email}).send()
	},
	getUsers: function() {
		var a = api.request({method: 'getUsers'})
		a.send()
		return a
	},
	getTasks: function() {
		var a = api.request({method: 'getTasks'}).onSuccess(function(ret){scratchCTF.tasks=ret.tasks})
		a.send()
		return a
	},
	joinTeam: function(teamid) {
		var a = api.request({method: 'joinTeam', teamid: teamid}).onSuccess(function(rt){routie('team/'+teamid);updateData()}).send()
	},
	leaveTeam: function() {
		var a = api.request({method: 'leaveTeam'}).onSuccess(function(){updateData()}).send()
	},
	setTeamCaptain: function(uid) {
		var a = api.request({method: 'setTeamCaptain', uid: uid}).send()
		ApiMethods.scoreboard()
		ApiMethods.getMyTeam(true)
	},
	kickFromTeam: function(uid) {
		var a = api.request({method: 'kickFromTeam', uid: uid}).send()
		ApiMethods.scoreboard()
		ApiMethods.getMyTeam(true)
	},
	renameTeam: function(name) {
		var a = api.request({method: 'renameTeam', name: name}).send()
	},
	usersInTeam: function(teamid) {
		var a = api.request({method: 'usersInTeam', teamid: teamid})
		a.send()
		return a
	},
	teamSolved: function(teamid) {
		var a = api.request({method: 'teamSolved', teamid: teamid})
		a.send()
		return a
	},
	sendFlag: function(taskid, flag) {
		var a = api.request({method: 'sendFlag', taskid: taskid, flag: flag}).send()
		ApiMethods.getTasks()
		ApiMethods.scoreboard()
	},
	getTeamInfo: function(teamid) {
		var a = api.request({method: 'getTeamInfo', 'teamid': teamid}).onSuccess(function(ret){scratchCTF['team'+teamid]=ret.team})
		a.send()
		ApiMethods.scoreboard()
		return a
	},
	getMyTeam: function(tomyteamafter) {
		if (tomyteamafter == undefined) {
			tomyteamafter = false
		}
					
		var a = api.request({method: 'getMyTeam'}).onSuccess(function(rt){
			scratchCTF.myteam=rt;			
			if (tomyteamafter){
				routie('team/'+scratchCTF.myteam.team.id)
			}
			myTeam();
		})
		a.send()
		return a
	},
	me: function() {
		var a = api.request({method: 'me'}).onSuccess(function(rt){scratchCTF.me=rt;myTeam()})
		a.send()
		return a
	},
	getInvites: function() {
		var a = api.request({method: 'getInvites'}).onSuccess(function(rt){scratchCTF.invites=rt.invites})
		a.send()
		return a
	},
	deauth: function() {
		var a = api.request({method: 'deauth'}).onSuccess(function(rt){Cookies.remove('session');Cookies.remove('state');window.location='/welcome'}).send()
	},
	cancelInvite: function(teamid) {
		var a = api.request({method: 'cancelInvite', teamid: teamid}).send()
		ApiMethods.getInvites()
	},
	getNews: function() {
		var a = api.request({method: 'getNews'}).onSuccess(function(rt){scratchCTF.news = rt.news;scratchCTF.news.sort((a)=>a.time);scratchCTF.news.reverse();}).send()
	}
}
