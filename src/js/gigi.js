var lambda = function(v) {return function() {return v}};
var fval = function(el) {while(typeof el == "function") el = el(); return el};
var $gi = {};
var gigiComponent = function(obj) {
    if(!obj.methods) obj.methods = function() {return {}};
    if(!obj.oninit) obj.oninit = function() {return {}};
    var fn = function(p) {
        var props = obj.props;
        if(p) for(var i in props) {if(props.hasOwnProperty(i) && p[i] === undefined) p[i] = props[i];}
        var el = () => gi({is: obj.element(p, el), on$created: function(e) {e.target.$gi = el}});
        var $ = el().$;
        el.isComponent = true;
        
        Object.defineProperty(el, "$", {
            get: function() {return $;}
        });
        
        Object.defineProperty(el, "props", {
            get: function() {return props;}
        });
        obj.oninit(props);
        defMethods(el, obj.methods(p, el));
        return el;
    };
    
    $gi[obj.name] = fn;
    return fn;
};

var defMethods = function(fn, o) {
    if(fn.methodsDefined) return;
    fn.methodsDefined = true;
    for(var i in o) {if(!o.hasOwnProperty(i)) continue;
        fn[i] = o[i];
    };
};

var applyProps = function(el, props) {
    var concatElems = function(l1, l2) {
        var norm = function(el) {return (el instanceof Array)? el: [el]}
        return (l1 === undefined)
            ?((l2 === undefined)? []      : norm(l2))
            :((l2 === undefined)? norm(l1): norm(l1).concat(norm(l2)))
    };
    
    var computeStyle = function(st) {
        if(st === undefined) return "";
        if(typeof st == "string") return st;
        var ret = "";
        for(var i in st) {if(!st.hasOwnProperty(i)) continue;
            ret += i + ":" + st[i] + ";";
        }
        return ret;
    }
    
    if(!el.events) el.events = {};
    if(!el.attrs) el.attrs = {};
    for(var i in props) { if(!props.hasOwnProperty(i) || i == "is" || i == "$") continue;
        //props[i] = fval(props[i]);
        if(i == "class") {
            var cl = fval(props[i]).replace(/\./g, " ").trim();
            if(el.attrs["class"])
                el.attrs["class"] += " " + cl;
            else
                el.attrs["class"] = cl;
        } else if(i == "id") {
            el.attrs.id = fval(props.id)
        } else if(i == "tag" || i == "key") {
            el[i] = fval(props[i]);
        } else if(i == "children") {
            el.children = concatElems(el.children, props[i]);
            for(var j = 0; j < el.children.length; j++) {
                el.children[j] = lambda(el.children[j]);
            }
        } else if(i.slice(0, 2) == "on") {
            el.events[i.slice(2)] = concatElems(el.events[i.slice(2)], props[i]);
        } else if(i == "style") {
            el.attrs.style = computeStyle(el.attrs.style) + computeStyle(fval(props.style));
        } else {
            el.attrs[i] = fval(props[i]);
        }
    };
    if(!el.attrs.id) delete el.attrs.id;
    return el;
};

var giQuery = function(parent, chlds, el) {    
    for(var i = 0; i < chlds.length;i++) {
        (function(i) {
            var ch = chlds[i];
            if(!ch || !ch.query) return;
            var q = ch.query;
            var queryEl = function(changes) {
                changes.is = ch;
                parent.oninit.push(function(ret) {
                    ret.children[i] = gi(changes);
                });
                return parent.root || parent;
            };
            ch.root = parent;
            queryEl.$ = ch.$;
            for(var i in queryEl.$) if(queryEl.$.hasOwnProperty(i)) Object.defineProperty(queryEl, i, {get: () => queryEl.$[i]});
            parent.$[q] = queryEl;
        }(i));
    };
};

var gi = function(blablabla) {
    var args = arguments;
    var el = args[0];
    var newel;
    var chlds = Array.prototype.slice.call(args, 1);
    var chldsQuery = {};
    if(el instanceof Array) newel = {children: el};
    if(el == "<" && el == "!") newel = {tag: el, children: chlds};
    if(typeof el == "string" || typeof el == "function") el = {is: el};
    if(typeof el.is == "function") {
        if(el.is.isComponent) el.is = el.is();
        el.children = chlds;
        chldsQuery = el.is.$;
        newel = applyProps(el.is(), el);
    }

    var applyQueryChanges = function(el) {
        for(var i = 0; i < self.oninit.length; i++) self.oninit[i](el, self);
        return el;
    };

    var self = function() {
        if(newel) return applyQueryChanges(newel);
        var ret = {
            events: {},
            attrs: {},
            children: chlds
        };
        
        var tgid = (el.is.match(/#\w*[-_&$+\d]*\w*|#\w*/g) || [""])[0].slice(1);
        var tgclass = (el.is.match(/\.\w*[-_&$+\d]*\w*|\.\w*/g) || [""]).join("").replace(/\./g, " ").trim();
        
        if(!el.id && tgid) el.id = tgid;
        if(el["class"] && tgclass) el["class"] = tgclass + " " + fval(el["class"]);
        else if(!el["class"] && tgclass) el["class"] = tgclass;
        el.tag = (el.is.match(/[-_&$]|\w+|\d*[#|\.|\D*]/) || [""])[0].replace(/[#|\.|\s*]/, '') || "div";
        applyProps(ret, el);
        return applyQueryChanges(ret);
    };
    if(!self.oninit) self.oninit = [];
    if(!self.$) self.$ = {};
    for(var i in chldsQuery) {if(!chldsQuery.hasOwnProperty(i)) continue;
        self.$[i] = chldsQuery[i];
    }
    giQuery(self, chlds, el);
    self.query = el.$;
    for(var i in self.$) if(self.$.hasOwnProperty(i)) Object.defineProperty(self, i, {get: () => self.$[i]});
    return self;
};
