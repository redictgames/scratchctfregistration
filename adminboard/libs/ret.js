var ret = function(resp) {
	var resp = JSON.parse(resp) || {'error': {'code': 'null response'}}

	if (resp.deauth) {
        exit()
    }

    if(resp.message) {
        notice.$m.show({message:resp.message, timeout:1500})
    }

	if(resp.error) {
		var error = resp.error
		console.log(error)
	}

	if(resp.cookies) {
		var cookies = resp.cookies

		for(var i in cookies) {
			Cookies.set(i, cookies[i])
		}		
	}

	return resp
}