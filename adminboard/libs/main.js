if (Cookies.get('adminsession') == undefined) login();
else {
	gigi.init();

	var notice = gigi_snackbar({})

	var addnewsb = addnews({})
	var addtaskb = addtask({})
	var changetaskb = changetask({})
	var changenewsb = changenews({})

	var page = [
		notice, 
		addnewsb, 
		changenewsb
	]
	gigi.rend(page)
}
