var typeOf = function(el) {
    return Object.prototype.toString.call(el).slice(8, -1);
};

var objmerge = function(obj1, obj2) {
    var obj3 = {};
    obj1 = obj1 || {};
    obj2 = obj2 || {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
};

var applyPropsFor = function(el, props, d) {
    var concatElems = function(els1, els2) {
        var norm = function(el) {
            return typeOf(el) == "Array"? el: [el];
        };
        if(els1 === undefined) return norm(els2);
        if(els2 === undefined) return norm(els1);
        return norm(els1).concat( norm(els2) );
    };
    
    var _apply = function(obj1, obj2, _concat) {
        if(_concat === undefined) _concat = true;
        for(var i in obj2) {
            var prop = obj2[i];
            if(obj1[i] === undefined) {obj1[i] = prop;}
            else {
                if(typeOf(prop) == "Object" && typeOf(obj1[i]) == "Object") {
                    if(i == "style") {
                        _concat = false;
                    }
                    _apply(obj1[i], prop, _concat);
                } else if(typeOf(prop) == "String" && typeOf(obj1[i]) == "String" && _concat) {
                    obj1[i] += prop;
                } else {
                    obj1[i] = prop;
                }
            }
        }
    };  
    
    if(d !== undefined) {
        if(typeOf(props.events) == "Object") {
            if(el.events === undefined) el.events = {};
            for(var i in props.events) {
                el.events[i] = concatElems(el.events[i], props.events[i]);
            };
        }
        
        if(props.children !== undefined) {
            if(el.children === undefined) el.children = [];
            el.children = concatElems(el.children, props.children);
        }
        
        if(typeOf(props.attrs) == "Object") {
            if(el.attrs === undefined) el.attrs = {};
            if(el.attrs.style === undefined) el.attrs.style = {};
            _apply(el.attrs, props.attrs, true);
        }
    } else {
        _apply(el, props, true);
    }
};

var gigi = {
    
};

var gigi_component = function(el) {
    if(el.$p === undefined) el.$p = {};
    applyPropsFor(el, el.$p, true);
    var init = !el.root.arguments[1];
    el.forceupdate = (el.forceupdate === undefined)? true: el.forceupdate;

    if(init) {
        el.dom = cito.vdom.create(el).dom;
        if(typeof el.oninit === "function") el.oninit();
    }

    el.update = function() {
        if(typeof el.onupdate === "function") el.onupdate();
        var updated = el.root(el.$p, true);
        cito.vdom.update(el, updated);
        updated.dom.root = el;
        return el;
    };
    
    el.remove = function() {
        cito.vdom.remove(el);
    };

    el.$css = function(obj) {
        applyPropsFor(el.$p, {attrs: {style: obj}}, true);
        return el;
    };
    
    el.$events = function(obj) {
        applyPropsFor(el.$p, {events: obj}, true);
        return el;
    };
    
    el.$contains = function(obj) {
        applyPropsFor(el.$p, {children: obj}, true);
        return el;
    };
    
    el.$insertProps = function(props) {
        applyPropsFor(el.$p, props, true);
        return el;
    };
    
    return el;
};


//--------------------  I    N    I    T  --------------------//

var rootElement;
gigi.init = function() {
    var root;
    rootNodes = [];
    var reqUpd = function(childs) {
        for(var i in childs) {
            var child = childs[i];
            if(child.is) {     
                if(child.forceupdate) child.update();
                child.dom.root = child;
            }
            var chchilds = child.children;
            if(chchilds === undefined) continue;
            chchilds = Array.isArray(chchilds)? chchilds: [chchilds];
            reqUpd(chchilds);
        }
    };
    
    gigi.rend = function(elems) {
        rootNodes = elems;
        cito.vdom.update(rootElement, root);        
        reqUpd([rootElement]);
    };

    root = function() {
        return {
            children: rootNodes
        }
    };

    rootElement = cito.vdom.append(document.body, root);
};
