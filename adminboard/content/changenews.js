function range(a) {
    var arr = []

    for(var i = 0;i<a;i++) {
        arr.push(i)
    }

    return arr
}

function tostr(a) {
    for(var i = 0;i<a.length;i++) {
        a[i] = String(a[i])
    }

    return a
}

var changenews = function(props) {  
    var getdata = function(resp) {
        props.data = resp.news
        console.log(props.data)
        self.update()
    }
    if(props.data == undefined) {
        api(getdata, {method: 'getNews'}) 
        props.data = []
    }

    var id_input = gigi_input({
        type: 'text',
        value: 'Id новости',
        readonly:true
    })

    var p = {
        items: tostr(range(props.data.length)), 
        scrollable: true,
        content: id_input,
        closeOnClick: true,
        selected: function(obj) {
            props.id = obj.textContent
            id_input.$m.setValue(obj.textContent)
            props.news = props.data[props.id].news
            news.$m.setValue(props.news)
            datepicker.$m.set(new Date(props.data[props.id].date*1000).toDateInputValue())
            deletebutton.update()
            submit.update()
        }            
    };

    var dropdown = gigi_dropdown(p);

    var button = gigi_button({
        content:'Change news', 
        raised:true, 
        shadowed: true,
        events: {
            click: function() {
                dialog.$m.show()
            }
        }
    })

    var news = gigi_input({
        type: 'text',
        label: 'Новость',
        floatingLabel: true,
        value: props.news,
        events: {
            input: function(e) {
                var value = e.target.value
                props.news = value

                submit.update()             
            }
        }
    })

    var currenttime = gigi_checkbox({label:'Использовать текущее время', value: true, func:function(){datepicker.update();submit.update()}})
    var datepicker = datetime({
        events:{
            input: function(e) {
                submit.update()
            }
        },
        disabled: function() {
            return (!currenttime.$p.value?undefined:true)
        }
    })

    var submit = gigi_button({
        content:'Принять',
        disabled: function (){
            return ((!!props.id && props.news != undefined && props.news.split(' ').join('') != "" && (!isNaN(datepicker.$m.get()) || currenttime.$p.value))?undefined:true)
        },
        events: {
            click: function(){
                date = (currenttime.$p.value?new Date().getTime()/1000:datepicker.$m.get())
                api(function(){}, {method:'changeNews', action: 'UPDATE', news: props.news, date: date, id: props.data[props.id].id})
            }
        } 
    })

    var deletebutton = gigi_button({
        content: 'Удалить',
        disabled: function (){
            return (!!props.id?undefined:true)
        },
        events: {
            click: function() {
                api(function(){}, {method:'changeNews', action: 'DELETE', id: props.data[props.id].id})
                id_input.$m.setValue('Id новости')
                news.$m.setValue('')
                datepicker.$m.set(undefined)
                props.news = undefined
                props.id = undefined
                submit.update()
                deletebutton.update()
            }
        }
    })

    var dialog = gigi_dialog({
        title: "Изменить новость",
        content: [dropdown, currenttime, datepicker, news],
        actions: [submit, deletebutton],
        backdrop: true,
        closeOnOutsideClick:true,
        width: 300
    });

    var self = gigi_component({        
        is: "change news",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            style: {
                margin: '10px',
                display: 'inline-block'
            }       
        },
        children: [button, dialog]
    });
    

    
    return self;
};
