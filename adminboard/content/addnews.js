Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 16);
});

var currentDate = new Date();
var timezoneOffset = currentDate.getTimezoneOffset() * 60 * 1000;

var datetime = function(props) {
    var self = gigi_component({        
        is: "datetime",
        $m: {
            get: function() {
                return (self.dom.valueAsNumber+timezoneOffset)/1000
            },
            set: function(value) {
                self.$p.value = value;
                self.update()
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: "input",
        attrs: {
            type: 'datetime-local',
            'value': props.value,
            disabled: (typeof(props.disabled) == 'function'?props.disabled():(props.disabled==undefined?false:props.disabled))     
        },
        events:props.events
    });
    

    
    return self;
}

var addnews = function(props) {   
    var button = gigi_button({
        content:'Add news', 
        raised:true, 
        shadowed: true,
        events: {
            click: function() {
                dialog.$m.show()
            }
        }
    })

    var news = gigi_input({
        type: 'text',
        label: 'Новость',
        floatingLabel: true,
        events: {
            input: function(e) {
                var value = e.target.value
                props.news = value

                submit.update()             
            }
        }
    })

    var currenttime = gigi_checkbox({label:'Использовать текущее время', value: true, func:function(){datepicker.update();submit.update()}})
    var datepicker = datetime({
    	events:{
    		input: function(e) {
    			submit.update()
    		}
    	},
    	disabled: function() {
    		return (!currenttime.$p.value?undefined:true)
    	}
    })

    var submit = gigi_button({
        content:'Принять',
        disabled: function (){
            return ((props.news != undefined && props.news.split(' ').join('') != "" && (!isNaN(datepicker.$m.get()) || currenttime.$p.value))?undefined:true)
        },
        events: {
            click: function(){
                date = (currenttime.$p.value?new Date().getTime()/1000:datepicker.$m.get())
                console.log(date)
                api(function(){}, {method:'createNews', news: props.news, date: date})
            }
        } 
    })

    var dialog = gigi_dialog({
        title: "Добавить новость",
        content: [currenttime, datepicker, news],
        actions: submit,
        backdrop: true,
        closeOnOutsideClick:true,
        width: 300
    });

    var self = gigi_component({        
        is: "add news",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            style: {
                margin: '10px',
                display: 'inline-block'
            }       
        },
        children: [button, dialog]
    });
    

    
    return self;
};
