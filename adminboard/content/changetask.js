var changetask = function(props) {   
    var button = gigi_button({
        content:'Change task', 
        raised:true, 
        shadowed: true
    })

    var self = gigi_component({        
        is: "change task",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            style: {
                margin: '10px',
                display: 'inline-block'
            }       
        },
        children: [button]
    });
    

    
    return self;
};