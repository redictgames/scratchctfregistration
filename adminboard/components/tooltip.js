var gigi_tooltip = function(props) {
    props.toolbox = (props.toolbox === undefined)? {width: 0, height: 0}: props.toolbox;
    props.contentbox = (props.contentbox === undefined)? {width: 0, height: 0}: props.contentbox;
    var num = String(Math.random());
    var rootclass = 'gigi-tooltip__tooltip';
    rootclass += (props.large?' gigi-tooltip__tooltip--large': '');
    rootclass += (props.active? " is-active": "");
    
    var content = {
        tag: "div",
        attrs: {class: "gigi-tooltip__content"},
        children: props.content,
        events: {
            mouseenter: function() {
                props.active = true; self.update();
            },
            mouseleave: function() {
                //props.toolbox = self.children[1].dom.getBoundingClientRect();
                props.active = false; self.update();
            }
        }
    };
    
    var tooltip = function(old) {
        var el = {
	        tag: 'div',
        	attrs: {
        	    class: rootclass,
	            style: {
	                left: Math.max(props.contentbox.left + props.contentbox.width / 2 - props.toolbox.width / 2, 0) + "px",
	                top: props.contentbox.top + props.contentbox.height + 10 + "px"
	            }
	        },
	        events: {
	            mouseenter: function() {
                    props.active = false; self.update();
                }
	        },
        	children: props.description
	    };
        if(old) props.toolbox = old.dom.getBoundingClientRect();
        else props.toolbox = {width: 0};
        return el;
    }
	props.onupdate = function() {
        props.contentbox = self.dom.getBoundingClientRect();
    };
    props.oninit = function() {
        setTimeout(function() {
            self.update();
        }, 150);
    };
    
    var self = gigi_component({
        is: "gigi-tooltip",
        $m: {},
        $p: props,
        
        tag: "div",
        attrs: {class: "gigi-tooltip"},
        root: arguments.callee,
    	children: [
    	    content,
            tooltip
    	]
    });
    
    return self;
};
