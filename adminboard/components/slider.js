var gigi_slider = function(props) {
    props.min = (props.min == undefined)? 0: props.min;
    props.max = (props.max == undefined)? 100: props.max;
    props.step = (props.max == undefined)? 1: props.step;
    var cleft = (props._cursorLeft == undefined)? ((props.max - props.min) / props.value) + "%": props._cursorLeft + "px";
    var cursor = function() {
        var c = 100 / (props.max - props.min) * props.value + "%";
        return {
            tag: "div",
            attrs: {
                class: "gigi-slider__cursor",
                style: {left: cleft, "background-color": props.active? "red": ""}
            },
            events: {
                mousedown: function() {
                    props.active = true;
                    self.update();
                },
                blur: function() {
                    props.active = false;
                    self.update();
                }
            }
        }
    };
    
    var moveCursor = function(x) {
        var box = self.dom.getBoundingClientRect();
        props._cursorLeft = x;
        self.update();
    };
    
    var self = gigi_component({
        is: "gigi-slider",
        $m: {
            setValue: function(val) {
                props.value = val;
                self.update();
            }
        },
        $p: props,

        root: arguments.callee,
    	tag: "div",
        attrs: {
            class: "gigi-slider",
            type: 'range'
        },
        events: {
            mouseup: function() {
                props.active = false;
                self.update();
            },
            mousemove: function(e) {
                moveCursor(e.clientX);
            }
        },
        children: cursor
    });
    
    return self;
};
