var gigi_button = function(props) {
    
    var classprop = "gigi-button mdl-button mdl-js-button";
    classprop += (props.raised?" mdl-button--raised": "");
    classprop += (props.fab?" mdl-button--fab": "");
    classprop += (props.mini_fab?" mdl-button--mini-fab": "");
    classprop += (props.icon?" mdl-button--icon": "");
    classprop += (props.colored?" mdl-button--colored": "");
    classprop += (props.primary?" mdl-button--primary": "");
    classprop += (props.accent?" mdl-button--accent": "");
    classprop += (props.shadowed?" gigi-button--shadowed": "");

    var self = gigi_component({        
        is: "gigi-button",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "button",
        attrs: {
            class: classprop,
            disabled: (typeof(props.disabled) == 'function'?props.disabled():(props.disabled==undefined?false:props.disabled))
        },
        children: props.content
    });
    

    
    return self;
};
