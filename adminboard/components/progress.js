var gigi_progress = function(props) {  
    var rootclass = "gigi-progress mdl-progress";
    rootclass += (props.indeterminate?" mdl-progress__indeterminate": "");
    var self = gigi_component({
        is: "gigi-progress",
        $m: {
            setProgress: function(value) {
                props.value = value;
                self.update();
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: 'div',
    	attrs: {
            class: rootclass
        },
        children: [
            {tag: "div", attrs: {class: "progressbar bar bar1", style: {width: props.value + "%"}}},
            {tag: "div", attrs: {class: "bufferbar bar bar2", style: {width: "100%"}}},
        ]
    });
    
    return self;
};
