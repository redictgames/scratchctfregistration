var gigi_spinner = function(props) {  
    
    var rootclass = "gigi-spinner mdl-spinner mdl-js-spinner is-active";
    rootclass += (props.single_color?' mdl-spinner--single-color': "");
    
    var self = gigi_component({
        is: "gigi-spinner",
        $m: {
            start: function() {
                self.dom.MaterialSpinner.start()
            },
            stop: function() {
                self.dom.MaterialSpinner.stop()
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: 'div',
    	attrs: {
            class: rootclass
        }
        
    });
    
    return self;
};
