var gigi_dialog = function(props) {
    props.closeOnOutsideClick = (props.closeOnOutsideClick === undefined)? true: props.closeOnOutsideClick;
    var bg = gigi_backdrop({
        delay: 0.1,
        events: {
            click: function(e) {

                if(e.target == bg.dom && props.closeOnOutsideClick) {  
                    bg.$p.content.update();
                    bg.$m.hide(); 
                }

            }
        },
        attrs: {class: " gigi-toplayer"}
    });
    
    
    var root = function(props) {
        var title = function() {
            return {
                tag: "h3",
                attrs: {
                    class: "mdl-dialog__title"
                },
                children: props.title || ""
            };
        };
        
        var content = function() {
            return {
                tag: "div",
                attrs: {
                    class: "mdl-dialog__content gigi-dialog__content"
                },
                children: props.content
            };
        };
        
        var actions = function() {
            return {
                tag: "div",
                attrs: {
                    class: "mdl-dialog__actions" + (props.fullWidthActions? " taskdialogsubmit": "")
                },
                children: props.actions
            };
        };
        
        var dialog = function() {
            return {
                tag: "div",
                attrs: {
                    class: "gigi-dialog mdl-dialog",
                    style: {width:props.width}
                },
                children: [
                    title,
                    content,
                    actions
                ]
            };
        };
        
        var self = gigi_component({
            is: "gigi-dialog",
            $m: {
                open: function() {
                    bg.$m.show();
                },
                close: function() {
                    bg.$m.hide();
                }
            },
            $p: props,
            oninit: function() {
                if(props.opened) {
                    setTimeout(function() {
                        self.$m.open();
                        self.update()
                    }, 100)
                }
            },
            
            root: arguments.callee,            
            children: [dialog]
        });
        bg.$p.content = self;
        return self;
    };
    var dialog = root(props);

    return bg;
}
