var gigi_tabs = function(props) {
    props.selected = (props.selected === undefined)? 0: props.selected;
    var tab = function(content, index) {
        var postfix = ((index == props.selected)? " is-active": "");
        return {
            tag: "a", 
            attrs: {
                class: "gigi-tabs__tab" + postfix
            },
            events: {
                click: function() {
                    props.selected = index;
                    self.update();
                }
            },
            children: content
        };
        
    };
    
    var tabs = function() {
        var ret = [];
        var index = 0;
        for(var i in props.panels) {
            ret.push(tab(i, index));
            index += 1;
        };
        return ret;
    };
    
    var panels = function() {
        var ret = [];
        var index = 0;
        for(var i in props.panels) {
            var panel = props.panels[i];
            var selectedItem = props.selected || 0;
            var postfix = ((index == selectedItem)? " visible": " invisible");
            ret.push({
                tag: "div",
                attrs: {
                    class: "gigi-tabs__panel" + postfix
                },
                children: {
                    tag: "div",
                    attrs: {class: "flexible"},
                    children: panel
                }
            });
            index += 1;
        };
        return {children: ret};
    };

    var bar = function() {
        return {
            tag: "div",
            attrs: {
                class: "mdl-tabs__tab-bar"
            },
            children: tabs
        }
    };
    
    var self = gigi_component({
        is: "gigi-tabs",
        $p: props,
        $m: {},
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            class: "gigi-tabs "
        },
        children: [
            bar,
            panels
        ]
    });
    
    
    
    return self;
};
