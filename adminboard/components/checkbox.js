var gigi_checkbox = function(props) {
    props.type = props.type || "checkbox";
	var checkbox = {
        tag: "input",
        attrs: {
            style: "opacity: 0",
            class: (props.type != 'radio')? "mdl-" + props.type + "__input": "mdl-" + props.type + "__button",
            type: 'checkbox',
        },
        events: objmerge(props.events, {
            change: function() {
                self.$p.value = !self.$p.value;
                props.focused = false;
                self.update();
                props.func()
            },
            focus: function(e) {
                props.focused = true;
                self.update();
            },
            blur: function(e) {
                props.focused = false;
                self.update();
            },
        })
    };
    
    var labelclass = "mdl-" + props.type;
    labelclass += (props.value?" is-checked": "");
    
    var span = function() {
        return {
            tag: 'span',
            attrs: {class: "mdl-" + props.type + "__label"},
            children: props.label
        };
    };
    
    var mdlEls = {
        "checkbox": {children: [
        {tag: "span", attrs: {class: "mdl-checkbox__focus-helper"}},
        {tag: "span", attrs: {class: "mdl-checkbox__box-outline"}, children: 
            {tag: "span", attrs: {class: "mdl-checkbox__tick-outline"}}
        }
        ]},
        "switch": {children: [
        {tag: "span", attrs: {class: "mdl-switch__track"}},
        {tag: "span", attrs: {class: "mdl-switch__thumb"}, children: 
            {tag: "span", attrs: {class: "mdl-checkbox__focus-helper"}}
        }
        ]}
    }[props.type];
    
    var label = function() {
        return {
            tag: 'label',
            attrs: {
                class: labelclass + 
                (props.focused? " is-focused": "")
            },
            children: [
                checkbox, 
                span, 
                mdlEls
            ]
        };
    };
    
    var self = gigi_component({
        is: "gigi-checkbox",
        $m: {},
        $p: props,
        
        root: arguments.callee,
    	tag: 'div',
    	attrs: {
    	    class: "gigi-checkbox",
            style: {
                display: 'inline-block'
            }
    	},
    	children: label
    });
    
    return self;
};
