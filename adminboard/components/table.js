var gigi_table = function(props) {
    var ths = function(th, istitle) {
        var a = []

        var title = {
            'font-weight': '900;',
            color: 'black;'
        }
        
        for(var i=0;i<th.length;i++) {
            a.push({tag:'th',attrs:{class:"mdl-data-table__cell--non-numeric", style:(istitle?title:{})},children:th[i]})
        }
        
        return {tag:'tr',children:a}
    }
    
    var tr = function(list) {
        var a = []
        
        for(var i=0;i<list.length;i++) {
            a.push(ths(list[i]))
        }
        
        return a
    }
    
    var head = {
        tag: 'thead',
        children: ths(props.titles, true)
    }   
    
    
    
    var body = {
        tag: 'tbody',
        children: tr(props.content, false)
    }

    var self = gigi_component({        
        is: "gigi-table",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "table",
        attrs: {
            class: 'mdl-data-table mdl-shadow--2dp',
            style: {
                width:'100%'
            }
        },
        children: [head, body]
    });
    
    return self;
};
