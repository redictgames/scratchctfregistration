var gigi_backdrop = function(props) {
    props.delay = (props.delay === undefined)? 0.2: props.delay;
    props.state = (props.state === undefined)? "closed": props.state;
    var st = props.state;
    var noop = function() {};    

    var evt = ({
        "opening": props.beforeopen || noop,
        "opened": props.onopen || noop,
        "closing": props.beforeclose || noop,
        "closed": props.onclose || noop,
        "openstart": noop,
    })[st]; evt();

    var config = ({
        "opening": [false, true],
        "opened": [true, true],
        "closing": [false, true],
        "closed": [false, false],
        "openstart": [true, false]
    })[st];
    
    props.showed = config[0], props.display = config[1] 
    var opacity = ({
        "closed": "0",
        "opening": "0.4",
        "opened": "1",
        "closing": "0",
        "openstart": "0.1"
    })[st];

    var self = gigi_component({
        is: "gigi-backdrop",
        $m: {
            show: function() {
                props.state = "openstart";
                self.update();
            },
            hide: function() {
                if(!props.showed) {
                    props.state = "closed"
                } else {
                    props.state = "closing"
                }
                self.update();
            },
            toggle: function() {
                if(props.state == "opened") {
                    self.$m.hide();
                } else {
                    self.$m.show();
                }
                self.update();
            }
        },
        $p: props,
        forceupdate: false,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            class: "gigi-backdrop", 
            style: {
                opacity: opacity,

                transition: "all " + props.delay + "s",
                display: (props.display? "flex": "none"),
                position:'fixed'
            }
        },
        events: {       
            transitionend: function() {
                if(st == "closing") {
                    props.state = "closed";
                    self.update();
                }
            }
        },
        children: props.content
    });
    

    setTimeout(function() {
        if(st == "openstart") {
            props.state = "opening";
            self.update();
        } else if(st == "opening") {
            setTimeout(function() {
                props.state = "opened";
                self.update();
            }, props.delay)
        }
    }, 50);
    return self;
}
