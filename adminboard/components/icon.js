var gigi_icon = function(props) {
    props.style = props.style || {};
    props.style.cursor = "pointer";
    
    var self = gigi_component({
        is: "gigi-icon",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: 'i',
        attrs: {class: "gigi-icon material-icons"},
        children: props.icon    
    });
    
    return self;
}
