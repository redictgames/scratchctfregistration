var gigi_ripple = function(props) {
    props.d = (props.d === undefined)? 0: props.d;
    var d = (props.active)? String(props.d): "";
    var self = gigi_component({
        is: "gigi-ripple",
        $p: props,
        $m: {
            ripple: function(x, y) {
                var parent = props.parent;
                if(!parent) return;
                var box = parent.dom.getBoundingClientRect();
                var selfbox = self.dom.getBoundingClientRect();
                var d = (props.d = Math.max(box.height, box.width))
                props.x = x - (box.left || 0) - d/2;
            	props.y = y - (box.top || 0) - d/2;
            	self.update();
            	props.active = true;
            	setTimeout(function() {
        	        self.update();
        	    }, 0)
            },
            bindTo: function(p) {
                props.parent = p;
                var mDown = function(e) {
                    self.$m.ripple(e.pageX, e.pageY);
                };
                applyPropsFor((p.is? p.$p: p), {
                    events: {
                        mousedown: mDown
                    },
                    attrs: {
                        style: {
                            overflow: " hidden",
                            position: " relative"
                        }
                    }
                });
                return self;
            },
            appendTo: function(p) {
                self.$m.bindTo(p);
                if(p.is) {
                    p.$p.children = (typeOf(p.$p.children) == "Array"? p.$p.children.concat(self): [self])
                } else {
                    p.children = (typeOf(p.children) == "Array"? p.children.concat(self): [self])
                }
                console.log(p);
                return p;
            }
        },
        root: arguments.callee,
        tag: "div",
        events: {
            transitionend: function() {
                if(!props.active) return;
                props.active = false;
                self.update();
            }
        },
        attrs: {
            class: "gigi-ripple" + (props.active? " active": ""),
            style: {
                background: props.color || "",
                height: String(d), width: String(d),
                left: String(props.x) || "",
                top: String(props.y) || ""
            }            
        }
    });
    
    return self;
};
