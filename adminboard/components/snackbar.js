var gigi_snackbar = function(props) {
    var text = function() {
        return {
            tag: 'div',
            attrs: {class: "mdl-snackbar__text"},
        }
    }
    var btn = function() {
        return {
            tag: 'button',
            attrs: {class: "mdl-snackbar__action"}
        }
    }
    
    var rootclass = "gigi-snackbar mdl-js-snackbar mdl-snackbar";
    
    var self = gigi_component({
        is: "gigi-snackbar",
        $m: {
            show: function(data) {
                self.dom.MaterialSnackbar.showSnackbar(data);
            }
        },
        $p: props,

        root: arguments.callee,        
        tag: "div",
        attrs: {
            class: rootclass
        },
        children: [text, btn]
    });
    
    return self;
};
