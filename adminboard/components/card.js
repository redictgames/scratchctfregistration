var gigi_card = function(props) {
    props.style = props.style || {};
    var title = function() {
        return {
            tag: 'div',
            attrs: {class: 'mdl-card__title', style: props.style.title || {}},
            children: {
                tag: 'h2', 
                attrs: {class: "mdl-card__title-text"},
                children: props.title
            }
        };
    };

    var description = function() {
        return {
            tag: "div",
            attrs: {class: "mdl-card__supporting-text", style: props.style.content || {}},
            children: props.content
        };
    };

    var bottom = function() {
        return (props.bottom)? {
            tag: 'div',
            attrs: {class: "mdl-card__actions mdl-card--border", style: props.style.bottom},
            children: props.bottom
        }: {};
    };

    var cardmenu = function() {
        return {
            tag: 'div',
            attrs: {class: "mdl-card__menu"},
            children: props.menu
        };
    };
            

    var rootclass = "gigi-card mdl-card";
    rootclass += ' mdl-shadow--'+ (props.shadowheight || 1) + 'dp';
    
    var self = gigi_component({
        is: "gigi-card",
        $m: {},
        $p: props,
        
        root: arguments.callee,
    	tag: 'div',
    	attrs: {
            style: props.style.main,
            class: rootclass
        },
        children: [
            title,
            description,
            bottom,
            cardmenu
        ],
        
    });
    
    return self;
};
