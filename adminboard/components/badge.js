var gigi_badge = function(props) {  
    
    var rootclass = "gigi-badge mdl-badge";
    rootclass += (props.icon?' material-icons': "");
    rootclass += (props.overlap?' mdl-badge--overlap': "");

    var self = gigi_component({
        is: "gigi-badge",
        $p: props,
        $m: {},
        
        root: arguments.callee,
        tag: 'table',
        attrs: {
            class: rootclass,
            'data-badge': props.value
        },
        children: props.content
    });
        
    return self;
};
