var gigi_drawerPanel = function(props) {
    props.opened = (props.opened === undefined)? false: props.opened;
    var bg = gigi_backdrop({
        delay: 0.3,
        drawer: drawer,
        
        events: {
            click: function() {
                bg.$m.hide();
            }
        }
    });
    
    var bindBgEvts = function() {
        bg.$p = objmerge(bg.$p, {
            beforeopen: function() {
                props.visible = true;
                bg.$p.drawer.update();
                setTimeout(function() {
                    props.opened = true;
                    bg.$p.drawer.update();
                }, 20);
            },
            beforeclose: function() {
                props.opened = false;
                bg.$p.drawer.update();
            },
            onclose: function() {
                props.visible = false;
                bg.$p.drawer.update();
            },
        });
    };
    
    var root = function(props) {
        var drawer = function() {
            var drawerclass = "gigi-drawerPanel__content";
            return {
                tag: "div",
                attrs: {class: drawerclass},
                children: props.content
            };
        };
        
        var self = gigi_component({
            is: "gigi-drawerPanel",
            $m: {
                open: function() {
                    if(props.opened) return;
                    bg.$m.show(); 
                },
                close: function() {
                    if(!props.opened) return;
                    bg.$m.hide();
                },
                toggle: function() {
                    if(props.opened) self.$m.close();
                    else self.$m.open();    
                }
            },
            oninit: function() {
                setTimeout(bg.update, 50)
                console.log("INIT", bg);
            },
            forceupdate: false,
            $p: props,
            
            root: arguments.callee,
            tag: "div",
            attrs: {style: "z-index: 4;", class: "gigi-drawerPanel" + 
            (props.visible? " visible": " invisible") + 
            (props.opened? " opened": " closed")},
            children: [bg, drawer]
        });
        bg.$p.drawer = self;
        
        return self;
    };

    var drawer = root(props);
    bg.$p.drawer = drawer;
    bindBgEvts();
    return drawer;  
};
