var gigi_dropdown_ul = function(props){
    var self = gigi_component({        
        is: "dropdown-ul",
        $m: {
            setContent: function(content) {
                self.$p.content = content
                self.update()
            },
            setsize: function(width, height) {
                self.$p.width = width
                self.$p.height = height
                self.update()
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: "ul",
        attrs: {
            class: 'gigi-dropdown',
            style: {
                width: props.width,
                height: props.height
            }              
        },
        children: props.content,

    });
    
    return self;
}

var gigi_dropdown = function(props) {
    var item = function(item) {
        return {
            tag: 'div',
            attrs: {
                class: "gigi-dropdown__item",
            },
            children: item,
        	events: {
        		click: function() {
                    if ('selected' in self.$p) self.$p.selected(this)
        			if (self.$p.closeOnClick) self.$m.close()               	
            	}
        	}
        };
    };
    
    var items = function() {
        var propsitems = ('searchitems' in props?props.searchitems:props.items)
        var itms = []
        for (var i =0;i<propsitems.length;i++) {
            itms.push(item(propsitems[i]))
        }
        return itms
    }

    var outline = function() {
        return {
            tag:'div',
            attrs: {
                class: 'gigi-dropdown__outline ',
                style: {
                    width: props.width,
                    height: props.height
                }
            }
        }
    }

    if (props.visible === undefined) {
        props.visible = false
    }

    var u = gigi_dropdown_ul({content: items()})

    var self = gigi_component({        
        is: "gigi-dropdown",
        $m: {
            resize: function(){
            	var elem = self.dom.getElementsByClassName('gigi-dropdown')[0] 
                if (self.$p.width == undefined) {           
                    self.$p.width = String(elem.offsetWidth);
                    self.$p.height = String(elem.offsetHeight);

                    self.update(); 
                }         
            },
            toggle: function() {
                self.$p.visible = !self.$p.visible
                self.update()
            },
            close: function() {
            	self.$p.visible = false
                self.update()
            },
            open: function() {
            	self.$p.visible = true
                self.update()
            },
            search: function(str) {  
                console.log(u)             
                var itms = []

                for(var i in self.$p.items) {
                    unregitm = self.$p.items[i].split(' ').join('').toLowerCase().replace('№', '') 
                    unregstr = str.split(' ').join('').toLowerCase().replace('№', '')  
                    if(unregitm.includes(unregstr)) {
                        itms.push(self.$p.items[i])
                    }

                }
                self.$p.searchitems = itms
                
                u.$m.setContent(items())
            },
            setItems: function(a) {
                self.$p.searchitems = itms
                
                u.$m.setContent(items())
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
    		style: {
    			display: 'inline-block'
    		}       		
    	},
        children: [{
        	tag: 'div',
        	attrs: {
        		style: {
        			display: 'inline-block'
        		}       		
        	},
        	events: {
        		click: function() {
                	self.$m.toggle()
            	}
        	},
        	children: props.content
        }, 
        {
        	tag: 'div',
	        attrs: {
	            class: 'gigi-dropdown__container '+ (props.visible?'visible':'')+ (props.scrollable?' scrollable':''),
	            style: {
	                width: (props.scrollable?'200px':props.width),
	                height: (props.scrollable?'200px':props.height),
	            }
	        },
	        children: [outline, u]
	        
        }],


        events: {
            DOMNodeInserted: function() {
                setTimeout(function() {self.$m.resize()}, 1)
            }
        }

    });
    
    return self;
};
