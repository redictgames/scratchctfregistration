var gigi_input = function(props) {
    if(props.valid == undefined) props.valid = true    
    var input = function() {
        var events = props.events || {};
        var main = {
            tag: "input",
            attrs: {
                class: "mdl-textfield__input gigi-textfield__input",
                type: props.type || "text",
                value: props.value || "",
                readonly: props.readonly || false,
                placeholder: props.placeholder || '' 
            },
            events: objmerge(events, {
                input: function(e) {
                    props.value = e.target.value;
                    props.valid = e.target.validity.valid; 
                    self.update();            
                    if("input" in events) events.input(e);
                },                      
                focus: function(e) {
                    if(props.readonly) return;
                    props.focused = true;
                    self.update();
                },
                blur: function(e) {
                    if(props.readonly) return;
                    props.focused = false;
                    self.update();
                },
            })
        };
        if(props.pattern) main.attrs.pattern = props.pattern;
        return main;
    };
    var label = function() {
        return {
            tag: "label",
            attrs: {
                class: "mdl-textfield__label gigi-textfield__label"
            },
            children: props.label
        }
    };
    var error = function() {
        return {
            tag: "span",
            attrs: {
                class: "mdl-textfield__error",
                style: {
                    'font-size': '11px;'
                }
            },
            children: props.error
        }
    };
    
    var self = gigi_component({
        is: "gigi-input",
        $p: props,
        $m: {
            setValue: function(value) {
                self.$p.value = value;
                self.update();
            }
        },
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            class: "gigi-input mdl-textfield full-width" + 
            (props.floatingLabel? " mdl-textfield--floating-label": "") + 
            (props.focused? " is-focused": "") + 
            (props.valid? "": " is-invalid")+
            (!!props.value? " is-dirty": ""),
        },
        children: [
            input,
            label,
            error
        ]
    });
    
    return self;
};
