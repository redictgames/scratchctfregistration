var gigi_fileAttach = function(props) {
    var defaultText = "File...";
    var label = (props.label !== undefined)? gigi_input({
        readonly: true,
        placeholder: props.label || defaultText,
        value: props.filename || "",
        attrs: {                         
            style: {
                width: 'calc(100% - 32px)!important'
            }
        }
    }): {};
    var icon = gigi_icon({icon: props.attachIcon || 'attach_file'})

    var input = cito.vdom.create({
        tag: 'input', 
        attrs: {
            type: 'file'
        }, 
        events: {
            change: function(e) {
                files = e.target.files
                
                if (files.length > 0) {
                    props.filename = files[0].name
                } else {
                    props.filename = ''
                }
                
                props.files = files
                
                self.update();

                // включить когда будет готов тултип
               
            }
        }
    });

    var btn = gigi_button({
        content: [
            icon,
        ],
        events: {},
        icon: true,
        primary: true
    })

    var elems = [label, btn];

    var tooltip = function() {
        return gigi_tooltip({
            content: {
                children: elems
            },
            description: props.filename || defaultText
        })
    };
    
    var self = gigi_component({        
        is: "gigi-fileAttach",
        $m: {},
        $p: props,
        
        root: arguments.callee,
        tag: "div",
        attrs: {
            class: "gigi-fileAttach",
            style: {display: "inline-block"}
        },
        events: {
            click: function() {
                input.dom.click();
            }
        },
        children: (props.label === undefined)? tooltip: elems
    });
    
    if(props.disabled) {
        self.disabled = "true";
    } else if("disabled" in self.attrs) {
        delete self.attrs.disabled;
    };
    
    return self;
};
