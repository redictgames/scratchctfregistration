var gigi_list = function(props) {  
    props.items = (props.items === undefined)? []: props.items;
    var list_item = function(item, index) {
        return {
            tag: 'div',
            attrs: {class: "gigi-list__item" + ((props.selected == index)? " selected": "")},
            events: {},
            children: item
        };
    };

    var items = function() {
        var ret = [];

        for(var i = 0; i < props.items.length; i++) {       
            var item = props.items[i];
            ret.push(list_item(item, i));
        };    

        return ret;
    };

    var self = gigi_component({
        is: "gigi-list",
        $m: {
            push: function(item) {
                props.items = props.items.concat([item]);
                self.update();
            },
            unshift: function(item) {
                props.items = ([item]).concat(props.items);
                self.update();
            },
            splice: function(i, w) {
                props.items.splice(i, w);
                self.update();
            },
            remove: function(i) {
                self.splice(i, 1);
            },
            pop: function(i) {
                var el = props.items.pop(i);
                self.update();
                return el;
            }
        },
        $p: props,
        
        root: arguments.callee,
        tag: 'div',
        attrs: {
            class: "gigi-list",
        },
        children: items()
    });

    return self;
};
